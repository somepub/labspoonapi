<?php 

  /** 
  * Labspoon 
  * Raspberry Pi Arduino Backend 
  */
  define("MAINDIR","/var/www/html/labspoonAPI/");


class Core{

  /** 
  * Function Lets us get the current selected model 
  */
  private function get_model(){
    return "uno";
  }

   /** 
  * Function Lets us get the current compile/execute folder 
  */
  private function get_path(){
    $model = $this->get_model();
    return "/var/www/html/labspoonAPI/build/$model/";
  }

  private function onPath(){
    $path = $this->get_path();
    chdir($path);
  }

  private function get_port(){
    return "ACM1";
  }

  private function get_hexfile(){
    return $this->get_path()."build-uno/uno.hex";
  }

   /** 
  * Function Lets us save ace editor content data 
  */
  public function save_file($data){
    $file = "program.ino";
    $this->onPath();
    $data = $data. PHP_EOL;

    $fh = fopen($file,"w");

    if($fh){
      fwrite($fh,$data);
      fclose($fh);
      return "success";
    }
    else{
      return "fail";
    }
  }


  private function debug($row){

    $search      = array(
			'/(main.cpp:(\d+):[\d:]* warning:)/',
			'/(main.cpp:(\d+): error:)/'
		);
		$replaceWith = array(
			'<span class="console_warning"><a onClick="javascript: Editor.consoleDebug(\'\',$2);">$1</a>',
			'<span class="console_error"><a onClick="javascript: Editor.consoleDebug(\'\',$2);">$1</a>'
		);
		$rtn['row']  = preg_replace($search, $replaceWith, $row);

		if ($rtn['row'] == $row) {
			$rtn['row'] .= '<br />';
		} else {
			$rtn['row'] .= '</span><br />';
		}
		return $rtn;
	}

   /** 
  * Function compiles the code � serverside
  */

  function compile(){
    $dummy = array();
    $logfile = "";
    $rtn = array(
      "compile_success" => true,
      "compile_payload" => ""
    );

    $this->onPath();
    exec("make". ' 2>&1 | tee -a' . $logfile,$dummy);

    if(end($dummy) != ""){
      $rtn["compile_success"] = false;
    }

    //var_dump(end($dummy));

    foreach ($dummy as $dum) {
      $rowData = $this->debug($dum);
      $rtn["compile_payload"] .= $rowData['row'];
    }

    $conf["more"] = false;

    if(!$conf["more"]){
      $rtn["compile_payload"] = substr($rtn["compile_payload"],1975);
    }


    return $rtn;
  }

  function upload(){
    $dummy = array();
    $logfile = "";
    $rtn = array(
      "upload_success" => true,
      "upload_payload" => ""
    );

    $this->onPath();


    $executeCommand = "/usr/share/arduino/hardware/tools/avrdude -C/usr/share/arduino/hardware/tools/avrdude.conf -v -v -v -v -patmega328p -carduino -P/dev/tty{port_device} -b115200 -D -Uflash:w:{hex_file}";

    $searchPattern  = [
      "/{port_device}/",
      "/{hex_file}/"
    ];
    $replacePattern = [
      $this->get_port(),
      $this->get_hexfile(),
    ];

    $outp = preg_replace($searchPattern, $replacePattern, $executeCommand);
    //echo $outp;

    exec($outp. ' 2>&1 | tee -a' . $logfile,$dummy);

    if(end($dummy) != ""){
      $rtn["upload_success"] = false;
    }

    foreach ($dummy as $dum) {
      $rowData = $this->debug($dum);
      $rtn["upload_payload"] .= $rowData['row'];
    }

    return $rtn;

  }

  function liveExecuteCommand($cmd)
{

    while (@ ob_end_flush()); // end all output buffers if any

    $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');

    $live_output     = "";
    $complete_output = "";

    while (!feof($proc))
    {
        $live_output     = fread($proc, 4096);
        $complete_output = $complete_output . $live_output;
        echo "<pre>$live_output</pre>";
        @ flush();
    }

    pclose($proc);

    // get exit status
    preg_match('/[0-9]+$/', $complete_output, $matches);

    // return exit status and intended output
    return array (
                    'exit_status'  => intval($matches[0]),
                    'output'       => str_replace("Exit status : " . $matches[0], '', $complete_output)
                 );
}


  function monitor(){
    $rtn = "";
    $logfile = "";

    $cmd = "python /home/xxx/test.py ACM1 9600";

    //$this->liveExecuteCommand($cmd);


    $serial = new phpSerial();
    // First we must specify the device. This works on both Linux and Windows (if
    // your Linux serial device is /dev/ttyS0 for COM1, etc.)
    $serial->deviceSet("/dev/ttyACM1");
    // Set for 9600-8-N-1 (no flow control)
    $serial->confBaudRate(9600); //Baud rate: 9600
    $serial->confParity("none");  //Parity (this is the "N" in "8-N-1")
    $serial->confCharacterLength(8); //Character length     (this is the "8" in "8-N-1")
    $serial->confStopBits(1);  //Stop bits (this is the "1" in "8-N-1")
    $serial->confFlowControl("none");
    // Then we need to open it
    $serial->deviceOpen();
    // Read data
    $read = $serial->readPort();
    // Print out the data
    $data = preg_split('/\s+/', $read);
    //print_r($data); // red and split the data by spaces to array
    $array = array_count_values($data); // count the array values
    $values = array_keys($array, max($array)); // count the maximum repeating value
    // If you want to change the configuration, the device must be closed.
    $serial->deviceClose();

    return $values[0];
    
  }

}

class Config{

 
}

?>