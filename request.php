<?php
 header("Access-Control-Allow-Origin: *");
error_reporting(E_ALL);
ini_set('display_errors', 1);

include("lib/php-serial/php_serial.class.php");
include("api.class.php");

$API = new Core();

$cmd = (isset($_REQUEST['cmd']))?$_REQUEST['cmd']:NULL;
$data = (isset($_REQUEST['data']))?$_REQUEST['data']:NULL;

if($cmd=="requestCompile"){
  $API->save_file($data);
  echo json_encode($API->compile());
}

if($cmd=="requestUpload"){
  echo json_encode($API->upload());
}

if($cmd =="monitor"){
  echo json_encode($API->monitor());
}


?>